//
//  GameScene.swift
//  snake_xcode
//
//  Created by Дмитрий Гапоненко on 29.07.2021.
//

import SpriteKit
import GameplayKit

struct CollisionCategory {
    static let Snake: UInt32 = 0x1 << 0 // 0001 = 1
    static let SnakeHead: UInt32 = 0x1 << 1 // 0010 = 2
    static let Apple: UInt32 = 0x1 << 2 // 0100 = 4
    static let EdgeBody: UInt32 = 0x1 << 3 // 1000 = 8
    // static let Wall: UInt32 = 0x1 << 4 // 10000 = 16
}

class GameScene: SKScene {    
    var snake: Snake?
    var gameFrameRect: CGRect = .zero
    var gameFrameView: SKShapeNode!
    
    override func didMove(to view: SKView) {
        backgroundColor = SKColor.systemBlue
        physicsWorld.gravity = CGVector(dx: 0, dy: 0)
        physicsBody = SKPhysicsBody(edgeLoopFrom: frame)
        physicsBody?.allowsRotation = false
        
        view.showsPhysics = true
        
        drawGameFrame()
        
        let counterClockWise = SKShapeNode()
        counterClockWise.path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: 45, height: 45)).cgPath
        counterClockWise.position = CGPoint(x: view.scene!.frame.minX + 30 , y: view.scene!.frame.minY + 30)
        counterClockWise.fillColor = UIColor.orange
        counterClockWise.strokeColor = UIColor.black
        counterClockWise.lineWidth = 5
        counterClockWise.name = "counterClockWise"
        self.addChild(counterClockWise)
        
        let clockButton = SKShapeNode()
        clockButton.path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: 45, height: 45)).cgPath
        clockButton.position = CGPoint(x: view.scene!.frame.minX + 80 , y: view.scene!.frame.minY + 30)
        clockButton.fillColor = UIColor.orange
        clockButton.strokeColor = UIColor.black
        clockButton.lineWidth = 5
        clockButton.name = "clockButton"
        self.addChild(clockButton)
        
        createApple()
        
        snake = Snake(atPoint: CGPoint(x: view.scene!.frame.midX, y: view.scene!.frame.midY))
        self.addChild(snake!)
        
        self.physicsWorld.contactDelegate = self
        self.physicsBody?.categoryBitMask = CollisionCategory.EdgeBody
        self.physicsBody?.collisionBitMask = CollisionCategory.Snake | CollisionCategory.SnakeHead
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let touchLocation = touch.location(in: self)
            
            guard let touchesNode = self.atPoint(touchLocation) as? SKShapeNode, touchesNode.name == "counterClockWise" || touchesNode.name == "clockButton" else {
                return
            }
            
            touchesNode.fillColor = .gray
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let touchLocation = touch.location(in: self)
            
            guard let touchesNode = self.atPoint(touchLocation) as? SKShapeNode, touchesNode.name == "counterClockWise" || touchesNode.name == "clockButton" else {
                return
            }
            
            touchesNode.fillColor = .blue
            
            if touchesNode.name == "counterClockWise" {
                snake!.moveCounterClockWise()
            } else if touchesNode.name == "clockButton" {
                snake!.moveClockWise()
            }
        }
    }
    
    private func stop() throws {
        snake = nil
        gameFrameView.removeAllChildren()
        return
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        snake!.move()
    }
    
    func createApple() {
        let randX = CGFloat(arc4random_uniform(UInt32(view!.scene!.frame.maxX + 50)))
        let randY = CGFloat(arc4random_uniform(UInt32(view!.scene!.frame.maxY + 50)))
    
        let apple = Apple(position: CGPoint(x: randX, y: randY))
        self.addChild(apple)
    }
    
    final func drawGameFrame() {
        gameFrameView = SKShapeNode(rect: gameFrameRect)
        gameFrameView.fillColor = .lightGray
        gameFrameView.lineWidth = 2
        gameFrameView.strokeColor = .green
        addChild(gameFrameView)
    }
}

extension GameScene: SKPhysicsContactDelegate {
    func didBegin(_ contact: SKPhysicsContact) {
        let bodies = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        let collisionObject = bodies - CollisionCategory.SnakeHead
        
        switch collisionObject {
        case CollisionCategory.Apple:
            let apple = contact.bodyA.node is Apple ? contact.bodyA.node : contact.bodyB.node
            snake?.addBodyPart()
            apple?.removeFromParent()
            createApple()
        case CollisionCategory.EdgeBody:
            do {
                try stop()
            } catch {
                // some logging + print("Hey user. here is the error. reload the game")
            }
            break
//        case CollisionCategory.Wall:
//            stop()
//            break
        default:
            break
        }
    }
}
