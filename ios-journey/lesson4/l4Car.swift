//
// Created by Дмитрий Гапоненко on 18.07.2021.
//

import Foundation

// protocol CarProtocol {
//     func makeAction()
// }

class l4Car {
    var brand: String
    var year: Int
    var engineStatus: engineStatus
    var windowsState: windowsState

    init(brand: String, year: Int, engineStatus: engineStatus, windowsState: windowsState) {
        self.brand = brand
        self.year = year
        self.engineStatus = engineStatus
        self.windowsState = windowsState
    }

    enum Actions {
        case setEngineStatus(engineStatus: engineStatus)
        case setWindowsState(windowsState: windowsState)
    }

    enum engineStatus {
        case on, off
    }

    enum windowsState {
        case open, halfOpen, closed
    }
}

class l4SportCar: l4Car {
    var passengersCount: Int = 0

    enum passengersCount: Int {
        case one = 1
        case two = 2
        case three = 3
    }

    enum Actions {
        case setEngineStatus(engineStatus: engineStatus)
        case setWindowsState(windowsState: windowsState)
    }

    func makeAction(action: Actions) {
        switch action {
            case .setEngineStatus(engineStatus: let engineStatus):
                self.engineStatus = engineStatus
            case .setWindowsState(windowsState: let windowsState):
                self.windowsState = windowsState
        }
    }

    func setPassengers(count: passengersCount) {
        switch count {
        case .one, .two, .three:
            return
                // passengersCount = count.rawValue
        }
    }
}

class l4TrunkCar: l4Car {
    var trailer: Bool = false

    enum trailer {
        case yes, no
    }

    func makeAction(action: Actions) {
        switch action {
            case .setEngineStatus(engineStatus: let engineStatus):
                self.engineStatus = engineStatus
            case .setWindowsState(windowsState: let windowsState):
                self.windowsState = windowsState
        }
    }

    func setTrailer(withTrailer: trailer) -> String {
        switch withTrailer {
            case .yes:
                return "Est'"
            case .no:
                return "Netu"
        }
    }
}
