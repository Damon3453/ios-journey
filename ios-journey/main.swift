//
//  ios-journey
//
//  Created by Дмитрий Гапоненко on 04.07.2021.
//

import Foundation

/**
 round incoming param
 - Parameter x:
 - Returns: Double
 */
func customRound(_ x: inout Double) -> Double {
    Double(round(1000 * x) / 1000)
}

/* Lesson 1 */
// let lesson1_task_1 = lesson1Task1()
// let lesson1_task_2 = lesson1Task2()
// let lesson1_task_3 = lesson1Task3()
//
// lesson1_task_1.getD()
// lesson1_task_2.getData()
// lesson1_task_3.getTotalVklad()

/* Lesson 2 */
// let lesson2_task1 = lesson2Task1()
// let lesson2_task2 = lesson2Task2()
// let lesson2_task34 = lesson2Task34()
// let lesson2_task5 = lesson2Task5()
// let lesson2_task6 = lesson2Task6()

// print(lesson2_task1.checkModTwo(a: 4))
// print(lesson2_task2.checkModThree(a: 4))
// print(lesson2_task34.changeArray())
// print(lesson2_task5.arrOfFibo())
// print(lesson2_task6.fillSimpleNums(n: 100))

/* Lesson 3 */
// var lesson3_task1 = SportCarStruct(
//         brand: "Skoda",
//         year: 2019,
//         backSpaceCurrentState: .empty(empty: "Empty"),
//         engineState: .on,
//         windows: .open,
//         backSpaceMaxVolume: 100.0
//     )
// lesson3_task1.setEngineState(action: .stop)
// lesson3_task1.setBackSpaceCurrentState(state: .filled(kg: 40.0))
// lesson3_task1.description()

/* Lesson 4 */
 // var lesson4_Sport = l4SportCar()
 // var lesson4_Truck = l4TrunkCar()

// lesson4_Sport.setBrand(brand: "Skoda")
// lesson4_Sport.setYear(year: 2019)
// lesson4_Sport.setPassengers(count: .two)

// lesson4_Truck.setBrand(brand: "KaMAZ")
// lesson4_Truck.setYear(year: 2000)
// lesson4_Truck.setTrailer(withTrailer: .yes)


// print(lesson4_Sport.brand!)
// print(lesson4_Truck.trailerExists)

/* Lesson5 */
// var lesson5_SportCar = l5SportCar(brand: "Skoda", year: 2019, engineStatus: .on, windowsStatus: .open, spoiler: false)
// var lesson5_TrunkCar = l5TrunkCar(brand: "Belaz", year: 2015, engineStatus: .off, windowsStatus: .closed, trunk: true)
// print(lesson5_SportCar.description)
// print(lesson5_TrunkCar.description)

/* Lesson 6 */
// print("\(index)")
// var q = Queue<Int>()
// q.jobs = [4,6,8]
// print(q.get(6, 10) as Any)
// print(q[1] as Any)
// print(q.pop() as Any)
