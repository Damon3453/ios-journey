//
// Created by Дмитрий Гапоненко on 25.07.2021.
//

import Foundation

protocol l5CarProtocol {
    var brand: String { get }
    var year: Int { get }
    var engineStatus: engineStatus { get set }
    var windowsStatus: windowsStatus { get set }
}

class l5Car: l5CarProtocol {
    var brand: String
    var year: Int
    var engineStatus: engineStatus
    var windowsStatus: windowsStatus

    init (brand: String, year: Int, engineStatus: engineStatus, windowsStatus: windowsStatus) {
        self.brand = brand
        self.year = year
        self.engineStatus = engineStatus
        self.windowsStatus = windowsStatus
    }
}

enum engineStatus: String {
    case on = "Звведён"
    case off = "Заглушен"
}

enum windowsStatus: String {
    case open = "Открыты"
    case halfOpen = "Приоткрыты"
    case closed = "Заркыты"
}

extension l5CarProtocol {
    mutating func changeWindowsStatus(position: windowsStatus) {
        windowsStatus = position
    }

    mutating func changeEngineStatus(status: engineStatus) {
        engineStatus = status
    }
}
