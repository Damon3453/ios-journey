//
// Created by Дмитрий Гапоненко on 25.07.2021.
//

import Foundation

class l5SportCar: l5Car {
    var spoiler: Bool

    init (brand: String, year: Int, engineStatus: engineStatus, windowsStatus: windowsStatus, spoiler: Bool) {
        self.spoiler = spoiler

        super.init(brand: brand, year: year, engineStatus: engineStatus, windowsStatus: windowsStatus)
        self.brand = brand
        self.year = year
        self.engineStatus = engineStatus
        self.windowsStatus = windowsStatus
    }
}

// extension l5SportCar: CustomStringConvertible {
//     public var description: String {
//         """
//         Sport.
//         Brand: \(lesson5_SportCar.brand),
//         God: \(lesson5_SportCar.year),
//         Engine: \(lesson5_SportCar.engineStatus.rawValue),
//         Windows: \(lesson5_SportCar.windowsStatus.rawValue),
//         Spoiler est'? \(lesson5_SportCar.spoiler)
//
//         """
//     }
// }