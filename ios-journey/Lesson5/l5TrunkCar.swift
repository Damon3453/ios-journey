//
// Created by Дмитрий Гапоненко on 25.07.2021.
//

import Foundation

class l5TrunkCar: l5Car {
    var trunk: Bool

    init (brand: String, year: Int, engineStatus: engineStatus, windowsStatus: windowsStatus, trunk: Bool) {
        self.trunk = trunk

        super.init(brand: brand, year: year, engineStatus: engineStatus, windowsStatus: windowsStatus)
        self.brand = brand
        self.year = year
        self.engineStatus = engineStatus
        self.windowsStatus = windowsStatus
    }
}

// extension l5TrunkCar: CustomStringConvertible {
//     var description: String {
//         """
//         Trunk.
//         Brand: \(lesson5_TrunkCar.brand),
//         God: \(lesson5_TrunkCar.year),
//         Engine: \(lesson5_TrunkCar.engineStatus.rawValue),
//         Windows: \(lesson5_TrunkCar.windowsStatus.rawValue),
//         Kuzov est'? \(lesson5_TrunkCar.trunk)
//         """
//     }
// }
