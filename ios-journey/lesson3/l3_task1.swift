//
// Created by Дмитрий Гапоненко on 15.07.2021.
//

import Foundation

let auto: String? = nil
let year: Int? = nil

enum engineState: String {
    case on = "On"
    case off = "Off"
}
enum windowsState: String {
    case open = "Open"
    case close = "Close"
}
enum engineAction {
    case start, stop
}
enum backSpace {
    case empty(empty: String)
    case full(full: String)
    case filled(kg: Double)
}

struct SportCarStruct {
    var brand: String
    var year: Int
    var backSpaceCurrentState: backSpace
    var engineState: engineState
    var windows: windowsState
    var backSpaceMaxVolume: Double

    mutating func setEngineState(action: engineAction) {
        switch action {
            case .start:
                engineState = .on
            case.stop:
                engineState = .off
        }
    }
    
    mutating func setBackSpaceCurrentState(state: backSpace) {
        switch state {
            case .empty:
                backSpaceCurrentState = .empty(empty: "Empty bagaj")
            case .full:
                backSpaceCurrentState = .full(full: "Full bagaj")
        default:
            backSpaceCurrentState = .filled(kg: 20)
        }
    }

    func description() {
        print(
            """
            Brand: \(brand),
            Year: \(year),
            Engine state now: \(engineState.rawValue),
            Backspace volume: \(backSpaceCurrentState),
            Max volume: \(backSpaceMaxVolume)
            """
        )
    }
}
