//
// Created by Дмитрий Гапоненко on 06.07.2021.
//

import Foundation

class lesson1Task2 {
    let a: Double = 3
    let b: Double = 5

    /**
     print S, P and C of (right) triangle
     */
    func getData() {
        /**
         sqrt(a * a + b * b)
         */
        var c: Double = sqrt(pow(a, 2) + pow(b, 2))
        /**
         a * b / 2
         */
        var s: Double = Double((a * b) / 2)
        /**
         a + b + c
         */
        var p: Double = a + b + c

        print("S = \(customRound(&s))")
        print("P = \(customRound(&p))")
        print("C = \(customRound(&c))")
    }
}
