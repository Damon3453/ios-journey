//
// Created by Дмитрий Гапоненко on 06.07.2021.
//

import Foundation

class lesson1Task3 {
    var sum: Double = 100
    let percent: Double = 5

    func getTotalVklad() {
        for _ in 1...5 {
            sum += ((sum * percent) / 100)
        }

        print("Summa vklada cherez 5 let: \(customRound(&sum))")
    }
}
