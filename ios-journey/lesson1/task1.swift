//
// Created by Дмитрий Гапоненко on 05.07.2021.
//

import Foundation

class lesson1Task1 {
    var a: Double = 3
    var b: Double = 7
    var c: Double = -6

    /**
     get discriminant from static data
     - Returns: Void
     */
    func getD() -> Void {
        let d: Double = pow(b, 2) - 4 * a * c

        if d < 0 {
            return print("D < 0")
        }
        if Int(d) == 0 {
            let x: Double = -b / (2 * a)

            return print("One x: ", x)
        } else {
            var x1: Double = (-b + sqrt(d)) / (2 * a)
            var x2: Double = (-b - sqrt(d)) / (2 * a)

            return print(
                    "x1: \(customRound(&x1))",
                    "x2: \(customRound(&x2))"
            )
        }
    }

    // TODO: vista (vieta)
    // if a == 1 || a % 2 == 0  {
    //    x1 + x2 = -b
    // }
}
