//
// Created by Дмитрий Гапоненко on 29.07.2021.
//

import Foundation

func findIndex<T: Equatable>(ofString searchVal: T, in arr: [T]) -> Int? {
    for (key, item) in arr.enumerated() {
        if item == searchVal {
            return key
        }
    }

    return nil
}

let arr = [1,2,3,4,5,6]
let index = findIndex(ofString: 1, in: arr)


struct Queue<T> {
    var jobs: [T] = []

    mutating func push(_ job: T) {
        jobs.append(job)
    }

    mutating func pop() -> T? {
        guard jobs.count > 0 else {
            return nil
        }
        return jobs.removeLast()
    }

    func get(_ key: Int, _ defaultVal: T?) -> T? {
        if (jobs.indices.contains(key)) {
            return jobs[key]
        }

        return defaultVal ?? nil
    }

    subscript(key: Int) -> Int? {
        if (jobs.indices.contains(key)) {
            return (jobs[key] as? Int)
        }

        return nil
    }
}
