//
// Created by Дмитрий Гапоненко on 11.07.2021.
//

import Foundation

class lesson2Task5 {
    var arr = [0, 1]

    func arrOfFibo() -> [Int] {
        while arr.count <= 50 {
            arr.append(arr[arr.count - 1] + arr[arr.count - 2])
        }
        arr.removeFirst(4)

        return arr
    }
}