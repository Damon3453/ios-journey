//
// Created by Дмитрий Гапоненко on 10.07.2021.
//

import Foundation

class lesson2Task34 {
    /**
     create casual array
     - Returns: [Int]
     */
    func createArray() -> [Int] {
        var arr = [Int]()

        for item in (1...100) {
            arr.append(item)
        }

        return arr
    }

    /**
     remove items % 2 = 0 && % 3 = 0
     */
    func changeArray() -> Array<Any> {
        let arr = createArray()

        return arr.filter { $0 % 2 != 0 && $0 % 3 != 0 }
    }
}
