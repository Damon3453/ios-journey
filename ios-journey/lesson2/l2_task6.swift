//
// Created by Дмитрий Гапоненко on 11.07.2021.
//

import Foundation

class lesson2Task6 {
    var p: Int = 2

    // FIXME: в задании опечатка "p + 2". Переделать на pˆ2

    func fillSimpleNums(n: Int) -> [Int] {
        var arr = [Int](2...n)

        for item in stride(from: p + 2, to: n, by: p) {
            arr.remove(at: arr.firstIndex(of: item)!)
        }
        for item in arr where item > p {
            p = item
        }
        if p < n {
            return fillSimpleNums(n: n)
        }

        return arr
    }
}